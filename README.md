[TOC]

# Fraud: A quick fraud detection shortlisting

This repository offers a quick data science pipeline for shortlisting models to classify fraudulent tracsactions.

![fraud demo](docs/fraud_notebooks.gif)

## Libraries

The main libraries used for this repository are Numpy, Pandas, Scikit-learn, Plotly, Matplotlib, and Jupyter.

## Features

* Handling highly imbalanced dataset with sampling (0.17% fraudulent tracsactions).
* Data preprocessing and hyperparameter searching using scikit-learn's Pipelines
    * Sampling method (repeated stratified K-fold, random over-sampling, and synthetic minority over-sampling) is one of the hyperparameters
* Experiment visualization using matplotlib, seaborn, and plotly

## Setup

Clone this repository.

```bash
git clone https://puntitra@bitbucket.org/puntitra/fraud.git

```

Change working directory to fraud.
```bash
cd fraud
```

Install packages. *Note:* Python3 is required. Also, using virtual environment is highly recommended.
```bash
python3 -m pip install requirements.txt
```

*Note:* environment.yml is also available if conda is preferred. Enter `conda env create -f environment.yml`

## Dataset

Raw dataset collected and transformed by Andrea Dal Pozzolo et al. ([MLG](https://mlg.ulb.ac.be/wordpress/) at Université Libre de Bruxelles). The dataset is available [here](https://datahub.io/machine-learning/creditcard#data). The converted dataset (feather format) is located under data directory.

## Read-only View

A static HTML file for each IPython notebook for this repository is available under notebooks directory.
