import numpy as np
import pandas as pd 
from matplotlib import pyplot as plt 
import seaborn as sns
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_curve, auc


def load_dataset(full_path, drop_list=None, target_column='Class', test_size=0.1, random_state=0):
    """
        Load dataset, drop column(s) and split dataset

        Arguments:
        full_path: dataset file path
        drop_list: list of attributes to drop
        target_column: target column name
        test_size: propotion of dataset to include in the test split range [0, 1]
        random_state: random seed

        Return:
        X_train, X_test, y_train, y_test
    """
    if drop_list is None: 
        drop_list = []
    if target_column.strip():
        drop_list.append(target_column)

    df = pd.read_feather(full_path)
    X = df.drop(np.array(drop_list), axis=1)
    y = df[[target_column]]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, 
                                                        random_state=random_state)
    return X_train, X_test, y_train, y_test


def get_class_distribution(df, target_column='Class'):
    """
    Return class distribution.

    Arguments
    df -- Pandas dataframe with at least one label column
    target_column -- (optional) label column name or index

    Return: 
    dist_dict -- dictionary of class distribution
    """    
    dist_dict = {}

    for each in df[target_column].unique():
        dist_dict[each] = df[target_column].value_counts()[each]/df.shape[0]

    return dist_dict


def plot_heat_map(df_corr, figsize=(20, 20), isHalf=True):
    """
        Plot heat map of correlation matrix.
        
        Arguments:
        df_corr -- Pandas correlation matrix
        figsize -- (optional) Figure size in tuple format (float, float)
        half -- (optional) plot diagonal matrix if True, otherwise plot full 
                matrix
    """
    _, _ = plt.subplots(figsize=figsize)
 
    # To display diagonal matrix instead of full matrix.
    mask = np.zeros_like(df_corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = isHalf
 
    # Generate a custom diverging colormap.
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
 
    # Draw the heatmap with the mask and correct aspect ratio.
    sns.heatmap(df_corr, mask=mask, cmap=cmap, vmax=1, center=0, annot=True, 
                fmt='.2f', square=True, linewidths=.5, cbar_kws={"shrink": .5})


def get_high_correlated_features(corr, threshold=0.95):
    """
        Return high correlated features above the threshold

        Arguments:
        corr -- correlation matrix
        threshold -- correlation threshold within range [0, 1]
    """
    corr = corr.abs()
    # Select diagonal matrix of correlation matrix
    upper = corr.where(np.triu(np.ones(corr.shape), k=1).astype(np.bool))
    
    # Find index of columns with correlation over the threshold
    to_drop = [col for col in upper.columns if any(upper[col] > threshold)]

    return to_drop


def get_constant_features(df):
    """
    Return features that have the same value in all examples.

    Argument:
    df -- Pandas dataframe

    Return:
    to_drop -- List of numerical features that have the same value in all 
               examples
    """
    # Select numerical features
    numerical_df = df[df.select_dtypes([np.number]).columns]

    variance_threshold_filter = VarianceThreshold(threshold=0)
    variance_threshold_filter.fit(numerical_df)

    # get the constant features.
    to_drop = [col for col in numerical_df.columns if col not in 
               numerical_df.columns[variance_threshold_filter.get_support()]]

    to_drop += [col for col in df.columns 
                if (df[col].dtype == 'O' and len(df[col].unique())  == 1 )]

    return to_drop


class NoSampler:
    """
    Do not perform any sampling. This class is used for baseline.
    """
    def sample(self, X, y):
        return X, y

    def fit(self, X, y):
        return self

    def fit_resample(self, X, y):
        return self.sample(X, y)
    

# May use sklearn.metrics.average_precision_score instead
def get_auc(y_true, probas_pred):
    """
    Return area under curve calculated from precision and recall

    Arguments:
    y_true -- True binary labels {-1, 1} or {0, 1}. Shape = [m]
    probas_pred -- Estimated probabilities. Shape = [m]
    """
    precision, recall, _ = precision_recall_curve(y_true, probas_pred)

    return auc(recall, precision)


def evaluate_model(y_true, y_score, p=0.5, verbose=True):
    """
    Return dictionary of metrics
    """
    metrics = {}
    cm = confusion_matrix(y_true, y_score > p)

    tn = cm[0][0]
    tp = cm[1][1]
    fn = cm[1][0]
    fp = cm[0][1]
    metrics['tn'] = tn
    metrics['tp'] = tp
    metrics['fn'] = fn
    metrics['fp'] = fp

    log = 'Missed fraudulent transactions (false negative): {}\n'.format(fn)
    log += 'Detected fraudulent transactions (true positive): {}\n'.format(fp)
    log += 'Valid transactions detected as fraudulent (false positive: {}\n'.format(fp)
    log += 'Detected valid transactions (true negative): {}\n'.format(tn)
    log += 'Fradulent transactions: {}, valid transactions: {}\n'.format(np.sum(cm[1]), np.sum(cm[0]))

    auc = roc_auc_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred>p)
    recall = recall_score(y_true, y_pred>p)
    mcc = matthews_corrcoef(y_true, y_pred)
    metrics['auc'] = auc
    metrics['precision'] = precision
    metrics['recall'] = recall_score
    metrics['mcc'] = mcc 

    log += 'AUC: {}\n'.format(auc)
    log += 'Precision: {}\n'.format(precision)
    log += 'Recall: {}\n'.format(recall)
    log += 'MCC: {}\n'.format(mcc)

    p, r, t = precision_recall_curve(y_true, y_pred>p)
    area = np.trapz(r, p)
    metrics['area'] = area
    
    log += 'Precision-Recall AUC: {}\n'.format(area)

    if verbose:
        print(log)

    return metrics
